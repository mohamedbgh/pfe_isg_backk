package org.sid.entities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;

import org.sid.entities.Client;

@Entity
public class Produit{
	@Id 
private Long produit_id;
private String nom;
private double prix;
private String description;

@ManyToMany(mappedBy = "produits")
private List<Client> books = new ArrayList<Client>();

//@ManyToMany(mappedBy = "likedproduct")
//private List<Client> Clients;



public Produit(Long produit_id, String nom, double prix, String description) {
	super();
	this.produit_id = produit_id;
	this.nom = nom;
	this.prix = prix;
	this.description = description;
}




public Produit() {
	super();
	// TODO Auto-generated constructor stub
}

public Long getProduit_id() {
	return produit_id;
}
public void setProduit_id(Long produit_id) {
	this.produit_id = produit_id;
}


public String getNom() {
	return nom;
}
public void setNom(String nom) {
	this.nom = nom;
}


public double getPrix() {
	return prix;
}
public void setPrix(double prix) {
	this.prix = prix;
}


//public Collection<Client> getClients() {
//	return clients;
//}
//public void setClients(Collection<Client> clients) {
	//this.clients = clients;
//}


public String getDescription() {
	return description;
}
public void setDescription(String description) {
	this.description = description;
}

}
