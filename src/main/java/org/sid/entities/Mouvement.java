package org.sid.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.sid.entities.Compte;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Mouvement {
	@Id @GeneratedValue
	private Long numero;
	private Date dateOperation;
	private String nature;
	private double montant;
	private String sens ;
	
	@ManyToOne
	@JoinColumn(name = "CODE_CPTE")
	private Compte compte;
	
	public Long getNumero() {
		return numero;
	}
	public void setNumero(Long numero) {
		this.numero = numero;
	}
	public Date getDateOperation() {
		return dateOperation;
	}
	public void setDateOperation(Date dateOperation) {
		this.dateOperation = dateOperation;
	}
	public double getMontant() {
		return montant;
	}
	public void setMontant(double montant) {
		this.montant = montant;
	}
	public Compte getCompte() {
		return compte;
	}
	public void setCompte(Compte compte) {
		this.compte = compte;
	}
	
	public String getSens() {
		return sens;
	}
	public void setSens(String sens) {
		this.sens = sens;
	}
	
	public String getNature() {
		return nature;
	}
	public void setNature(String nature) {
		this.nature = nature;
	}
	public Mouvement() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Mouvement( Date dateOperation,String nature, double montant, Compte compte, String sens) {
		super();
		this.dateOperation = dateOperation;
		this.nature = nature;
		this.montant = montant;
		this.compte = compte;
		this.sens = sens;
	}
	
	
}
