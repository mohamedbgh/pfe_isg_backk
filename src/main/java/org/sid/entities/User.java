package org.sid.entities;

import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class User {
	@Id @GeneratedValue
	private Long id_User;
	private String username;
	private String password;
	private String role;
	
//	@ManyToMany
//	 @JoinTable(
//	   name = "client_géré", 
//	   joinColumns = @JoinColumn(name = "id_User"), 
//	   inverseJoinColumns = @JoinColumn(name = "id"))
//	 private Collection<Client> géré_client ;
//	
	
	
	
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public User(String username, String password, String role) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
	}


	public Long getId() {
		return id_User;
	}
	public void setId(Long id) {
		this.id_User = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	
	
}
