package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.sid.entities.Compte;
import org.sid.entities.Produit;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
public class Client {
	@Id @GeneratedValue
	private Long id;
	private String nom;
	private String prenom;
	private String dateNaissance;
	private String Mail;
	 
	 @JsonIgnore
	 @OneToMany(mappedBy="client",fetch=FetchType.LAZY )
		private List<Compte> comptes ;
	 
	 @ManyToMany(cascade = CascadeType.ALL)
	    @JoinTable(name = "client_produit",
	        joinColumns = @JoinColumn(name = "client_id", referencedColumnName = "id"),
	        inverseJoinColumns = @JoinColumn(name = "produit_id", referencedColumnName = "produit_id"))
	    private List<Produit> produits;

// @ManyToMany
// @JoinTable(
//   name = "produit_like", 
//   joinColumns = @JoinColumn(name = "id"), 
//   inverseJoinColumns = @JoinColumn(name = "id_produit"))
// private List<Produit> likedproduct ;
//
//
// 
// @ManyToMany(mappedBy = "géré_client")
//	 private List<User> Users;
//	 
	
	
	public Client() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Client(String nom,String prenom, String dateNaissance, String mail) {
		super();
		this.nom =nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.Mail = mail;
	}
	

	public Client( String nom, String prenom, String dateNaissance, String mail, List<Produit> produits) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance;
		this.Mail = mail;
		this.produits = produits;
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getDateNaissance() {
		return dateNaissance;
	}
	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}
	public String getMail() {
		return Mail;
	}
	public void setMail(String mail) {
		Mail = mail;
	}



	public List<Compte> getComptes() {
	return comptes;
	}

public void setComptes(List<Compte> comptes) {
		this.comptes = comptes;
		}

	//public Collection<Produit> getProduit() {
			//return produit;
		//}

	//public void setProduit(Collection<Produit> produit) {
		//this.produit = produit;
		//}
	
	
	}
	
	