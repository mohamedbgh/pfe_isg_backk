package org.sid.entities;


import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.sid.entities.Client;
import org.sid.entities.Mouvement;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
//@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Compte  {
	@Id 
	private Long codeCompte;
	private String dateCreation;
	private double solde ; 
    @ManyToOne
	@JoinColumn( name = "CODE_CLI", referencedColumnName = "id")
	private Client client;
	
    @JsonIgnore
	@OneToMany(mappedBy = "compte",fetch=FetchType.LAZY)
	private List<Mouvement> mouvements;

	public Compte() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Compte(Long codeCompte, String dateCreation, double solde, Client client) {
		super();
		this.codeCompte = codeCompte;
		this.dateCreation = dateCreation;
		this.solde = solde;
		this.client = client;
		
	}
	

	public Compte(Long codeCompte, String dateCreation, double solde) {
		super();
		this.codeCompte = codeCompte;
		this.dateCreation = dateCreation;
		this.solde = solde;
		
		
	}
	public Long getCodeCompte() {
		return codeCompte;
	}

	public void setCodeCompte(Long codeCompte) {
		this.codeCompte = codeCompte;
	}

	public String getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(String dateCreation) {
		this.dateCreation = dateCreation;
	}

	public double getSolde() {
		return solde;
	}

	public void setSolde(double solde) {
		this.solde = solde;
	}
	

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
	this.client = client;
	}

public List<Mouvement> getMouvements() {
		return mouvements;
	}

	public void setMouvements(List<Mouvement> mouvements) {
		this.mouvements = mouvements;
	}
	

}