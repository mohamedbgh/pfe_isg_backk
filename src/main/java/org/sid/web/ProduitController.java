package org.sid.web;

import java.util.List;

import org.sid.dao.ProduitRepository;
import org.sid.entities.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class ProduitController {
	
	@Autowired
	private ProduitRepository produitRepository;
		
		@RequestMapping(value = "/Produits", method = RequestMethod.GET)
		public List<Produit> getProduits(){
			return produitRepository.findAll();
		}

		@RequestMapping(value = "/Produit/{id}", method = RequestMethod.GET)
		public Produit getProduit(@PathVariable Long id){
			return produitRepository.findById(id).get();
		}
		@RequestMapping(value = "/Produits/{nom}", method = RequestMethod.GET)
		public Produit getproduitByame(@PathVariable String nom){
			return produitRepository.findBynom(nom);
		}
		

		@RequestMapping(value = "/Produits", method = RequestMethod.POST)
		public Produit save(@RequestBody Produit P){
			return produitRepository.save(P);
		}
		
		@RequestMapping(value = "/Produits/{id}", method = RequestMethod.DELETE)
		public boolean supprimer(@PathVariable Long id){
			 produitRepository.deleteById(id);
			 return true;
		}
		
		
		@RequestMapping(value = "/Produits/{id}", method = RequestMethod.PUT)
		public Produit save(@PathVariable Long id,@RequestBody Produit P){
			P.setProduit_id(id);
			return produitRepository.save(P);
		}
		

	}

