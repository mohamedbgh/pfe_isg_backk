package org.sid.web;


import java.util.List;


import org.sid.dao.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.sid.entities.Client;

@RestController
@CrossOrigin
public class ClientController {
	@Autowired
	private ClientRepository clientRepository;
	
	@RequestMapping(value = "/Clients", method = RequestMethod.GET)
	public List<Client> getClients(){
		return clientRepository.findAll();
	}

	@RequestMapping(value = "/Client/{id}", method = RequestMethod.GET)
	public Client getClient(@PathVariable Long id){
		return clientRepository.findById(id).get();
	}
//	@RequestMapping(value = "/Clients/{username}", method = RequestMethod.GET)
//	public Client getClientByUserame(@PathVariable String username){
//		return clientRepository.findByUsername(username);
//	}
//	

	@RequestMapping(value = "/Clients", method = RequestMethod.POST)
	public Client save(@RequestBody Client C){
		return clientRepository.save(C);
	}
	
	@RequestMapping(value = "/Clients/{id}", method = RequestMethod.DELETE)
	public boolean supprimer(@PathVariable Long id){
		 clientRepository.deleteById(id);
		 return true;
	}
	
	
	@RequestMapping(value = "/Clients/{id}", method = RequestMethod.PUT)
	public Client save(@PathVariable Long id,@RequestBody Client C){
		C.setId(id);
		return clientRepository.save(C);
	}
	

}
