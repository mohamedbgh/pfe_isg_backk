package org.sid.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import org.sid.dao.CompteRepository;
import org.sid.entities.Client;
import org.sid.entities.Compte;

@RestController
@CrossOrigin
public class CompteController {

	@Autowired
	private CompteRepository Compterepos ;
	
	@RequestMapping(value = "/Comptes", method = RequestMethod.GET)
	public List<Compte> getComptes(){
		return Compterepos.findAll();
	}

	
	@RequestMapping(value = "/Compte/{id}", method = RequestMethod.GET)
public Compte getClient(@PathVariable Long id){
	return Compterepos.findById(id).get();
}
	
	@RequestMapping(value = "/CompteAdd", method = RequestMethod.POST)
	public Compte save(@RequestBody Compte C){
		return Compterepos.save(C);
	}
	
	@RequestMapping(value = "/ComptetDeletebyId/{id}", method = RequestMethod.DELETE)
	public boolean supprimer(@PathVariable Long id){
		 Compterepos.deleteById(id);
		 return true;
}
	
	@RequestMapping(value = "/CompteUpdate/{id}", method = RequestMethod.PUT)
	public Compte save(@PathVariable Long id,@RequestBody Compte C){
		C.setCodeCompte(id);
		return Compterepos.save(C);
	}
}
