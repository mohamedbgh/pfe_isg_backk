package org.sid.web;

import java.util.List;

import org.sid.dao.MouvementRepository;
import org.sid.entities.Compte;
import org.sid.entities.Mouvement;
import org.sid.metier.MouvementMetierImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
public class MouvementController {
	
	@Autowired
	private MouvementRepository mouvementrepos;
	private MouvementMetierImpl mvIpml;
	
	@RequestMapping(value = "/Mouvements", method = RequestMethod.GET)
	public List<Mouvement> getMouvements(){
		return mouvementrepos.findAll();
	}
	
	
//	@RequestMapping(value = "/Mouvement/{codeCompte}" , method = RequestMethod.GET)
//	public List<Mouvement> getMouvementparCompte(@PathVariable Long codeCompte){
//		Compte cp= mvIpml.getCompte(codeCompte);
//		return Compte.getMouvements();
//		
//	}
	

}
