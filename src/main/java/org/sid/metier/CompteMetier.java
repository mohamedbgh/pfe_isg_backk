package org.sid.metier;

import java.util.List;

import org.sid.entities.Compte;

public interface CompteMetier {
	public Compte saveCompte( Compte cp );
    public Compte getCompte(Long code);
    public Compte consulterCompte(Long codeCpte) ;
  
}
