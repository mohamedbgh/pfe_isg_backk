package org.sid.metier;

import org.sid.entities.Compte;

public interface MouvementMetier {
	 public Compte getCompte(Long codeCompte);
	 public void versement(Long codeCompte, double montant );
	 public void retrait(Long codeCompte, double montant );
	 public void virement(Long codeCompteRetrait,Long codeCompteVersement,double montant, String objet);
    
}
