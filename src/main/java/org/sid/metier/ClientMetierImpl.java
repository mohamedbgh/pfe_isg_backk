package org.sid.metier;

import java.util.List;

import org.sid.entities.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sid.dao.ClientRepository;

@Service
public class ClientMetierImpl implements ClientMetier{
	
	@Autowired
	private ClientRepository clientRepository;

	@Override
	public Client saveClient(Client c) {
		return clientRepository.save(c);
	}

	@Override
	public List<Client> listeClient() {
		return clientRepository.findAll();
	}

}
