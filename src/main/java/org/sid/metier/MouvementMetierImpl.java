package org.sid.metier;

import org.sid.entities.Compte;
import org.sid.entities.Mouvement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.Date;
import java.util.Scanner;

import org.sid.dao.CompteRepository;
import org.sid.dao.MouvementRepository;

@Service
public class MouvementMetierImpl implements MouvementMetier{
	
	@Autowired
	private MouvementRepository mouvementrepository;
	private CompteRepository CompteRepository;

	@Override
	public Compte getCompte(Long codeCompte) {
		Compte compte=CompteRepository.findById(codeCompte).get();
		 if (compte==null) throw new RuntimeException("Compte introuvable"); 
		 return compte;
	}

	@Override
	public void versement(Long codeCompte, double montant) {
		Compte compte = getCompte(codeCompte);
		Mouvement versement = new  Mouvement(new Date(), "espèce", montant, compte, "dépot");
	   mouvementrepository.save(versement); // ici, la methode save() permet l'enregistrement
	    //mettre a jour le solde du compte
	    compte.setSolde(compte.getSolde() + montant);
	    CompteRepository.save(compte); // ici, la methode save permet de mettre a jours le compte (update)  ---->Meme dans la console, on aura comme requette : Hibernate: update compte set code_cli=?, date_creation=?, solde=?, decouvert=? where code_compte=?
	 }

	@Override
	public void retrait(Long codeCompte, double montant) {
		Compte compte = getCompte(codeCompte);
		
		
		 if ( compte.getSolde() < montant )  throw new RuntimeException("Solde insuffisant");
		
		 else {
			 Mouvement retrait = new  Mouvement(new Date(), "espèce", montant, compte, "Retrait"); 
			    mouvementrepository.save(retrait); // ici, la methode save() permet l'enregistrement
			    //mettre a jour le solde du compte
			    compte.setSolde(compte.getSolde() - montant);
			    CompteRepository.save(compte); // ici, la methode save permet de mettre a jours le compte (update)
		}
	}

	@Override
	public void virement(Long codeCompteRetrait, Long codeCompteVersement, double montant, String objet) {
		if(codeCompteRetrait == codeCompteVersement)throw new RuntimeException("Impossible : On ne peut pas effectuer un virement dans le meme compte");
		Scanner sc = new Scanner(System.in);
		System.out.println("Veuillez saisir l'objet du virement :");
		objet = sc.nextLine();
		retrait(codeCompteRetrait,montant);
		versement(codeCompteVersement,montant);
	}

}
