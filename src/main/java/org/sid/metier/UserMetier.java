package org.sid.metier;

import java.util.List;

import org.sid.entities.User;

public interface UserMetier {
	public User saveUser(User u);
	public List<User> listeUser();
}
