package org.sid.metier;

import org.sid.entities.Produit;

public interface ProduitMetier {
	public Produit saveProduit(Produit p );
    public Produit getProduit(Long id);
    public Produit consulterProduit(Long id) ;
}
