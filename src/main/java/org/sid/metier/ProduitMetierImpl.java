package org.sid.metier;

import org.sid.dao.ProduitRepository;
import org.sid.entities.Produit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProduitMetierImpl implements ProduitMetier{

	@Autowired
    private ProduitRepository produitRepository;

	
	

	@Override
	public Produit saveProduit(Produit p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Produit getProduit(Long id) {
		return produitRepository.findById(id).get();
	}

	@Override
	public Produit consulterProduit(Long id) {
		Produit p =produitRepository.findById(id).get();
		if(p==null) throw new RuntimeException("Compte introuvable");
		return null;
	}
}
