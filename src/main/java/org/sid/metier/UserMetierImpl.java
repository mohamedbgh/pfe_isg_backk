package org.sid.metier;

import java.util.List;

import org.sid.dao.UserRepository;
import org.sid.entities.User;
import org.springframework.beans.factory.annotation.Autowired;

public class UserMetierImpl implements UserMetier{
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public User saveUser(User u) {
		return userRepository.save(u);
	}

	@Override
	public List<User> listeUser() {
		return userRepository.findAll();
	}

}
