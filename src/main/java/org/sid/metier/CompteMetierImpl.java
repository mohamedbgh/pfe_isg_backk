package org.sid.metier;

import org.sid.entities.Compte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import org.sid.dao.CompteRepository;

@Service
public class CompteMetierImpl implements CompteMetier{
	
	@Autowired
	private CompteRepository compteRepository;

	@Override
	public Compte saveCompte(Compte cp) {
        return compteRepository.save( cp );
			}

	@Override
	public Compte getCompte(Long code) {
		return compteRepository.findById(code).get();
	}

	@Override
	public Compte consulterCompte(Long codeCpte) {
		Compte cp =compteRepository.findById(codeCpte).get();
		if(cp==null) throw new RuntimeException("Compte introuvable");
		return null;
	}
	

}
