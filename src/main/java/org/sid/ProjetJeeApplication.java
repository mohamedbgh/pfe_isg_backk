package org.sid;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.sid.dao.ClientRepository;
import org.sid.dao.CompteRepository;
import org.sid.dao.MouvementRepository;
import org.sid.dao.ProduitRepository;
import org.sid.dao.UserRepository;
import org.sid.entities.Client;
import org.sid.entities.Compte;
import org.sid.entities.Mouvement;
import org.sid.entities.Produit;
import org.sid.entities.User;
import org.sid.metier.UserMetier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ProjetJeeApplication implements CommandLineRunner {
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private MouvementRepository mouvementRepository;
	@Autowired
	private ProduitRepository produitRepository;
	@Autowired
	private UserRepository userRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(ProjetJeeApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		
		
//	User u1=userRepository.save(new User("medbgh", "0000", "admin"));
//	User u2=userRepository.save(new User("nouhabgh", "1111", "Marketeur"));
//	
//	Client c1=clientRepository.save(new Client("ali","benali","04/10/1998","ali.benali@gmail.com"));
//	Client c2=clientRepository.save(new Client("salah","bensalah","09/10/2000","salah.bensalah@gmail.com"));
//	Client c3=clientRepository.save(new Client("james", "lebron", "07/05/1977","lebron.james@gmail.com"));
//		
//		
//	Compte cp1=compteRepository.save(new Compte(1234L, "25/05/2008", 69875,c1));
//		
//
//	    Mouvement mv1=mouvementRepository.save(new Mouvement(new Date(), "chéque", 300, cp1, "Retrait"));
//		
//		Produit p1=produitRepository.save(new Produit(1254L,"P1", 50000, "crédit"));
//		Produit p2=produitRepository.save(new Produit(34567L,"P2", 600, "nouvelles cartes"));
//		Produit p3=produitRepository.save(new Produit(56987L, "P3", 3000, "chéquier"));
//		Produit p4=produitRepository.save(new Produit(5987456L, "P4", 5000, "crédit"));
//		Produit p5=produitRepository.save(new Produit(789654L, "P5", 250, "crédit immobilier"));
	}

}
